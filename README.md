# Playground for a canvas animation

The goal of this project is to create a simple canvas animation that is triggered by UI items.

Current status:

![Movement on click, without the animation](flutter-canvas-no-animation.gif)

- Each time the "Go" button is pressed, the square moves one step to the right.

Goal:

- The movent of the square is smooth and the distance is covered in two seconds.
- While the square is moving, the "Go" button is disabled.
