import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Canvas Animation Playground',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Canvas Animation Playground'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final _square = ValueNotifier<Square>(Square());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: OutlinedButton(
                onPressed: () => _square.value.go(),
                child: const Text('Go'),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: FractionallySizedBox(
                  widthFactor: 1.0,
                  heightFactor: 1.0,
                  child: CustomPaint(
                    painter: AnimatedPainter(notifier: _square, context: context),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Square extends ChangeNotifier {
  final int stepsCount = 10;
  int step = 0;
  void go() {
    step = (step + 1) % stepsCount;
    notifyListeners();
  }
}

class AnimatedPainter extends CustomPainter {
  BuildContext context;
  ValueNotifier<Square> notifier;

  AnimatedPainter({required this.notifier, required this.context});

  @override
  void paint(Canvas canvas, Size size) {
    final cell = size.width / notifier.value.stepsCount;
    var strokePaint = Paint()
      ..color = const Color(0xff638965)
      ..style = PaintingStyle.stroke;
    var filledPaint = Paint()
      ..color = const Color(0xff638965)
      ..style = PaintingStyle.fill;

    canvas.drawRect(const Offset(0, 0) & size, strokePaint);
    canvas.drawRect(Offset(notifier.value.step * cell, cell) & Size(cell, cell), filledPaint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
